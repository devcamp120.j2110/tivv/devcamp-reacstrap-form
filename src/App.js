import ReactStrap from "./components/reactstrap-form";
import 'bootstrap/dist/css/bootstrap.min.css';


function App() {
  return (
    <div>
      <ReactStrap/>
    </div>
  );
}

export default App;
