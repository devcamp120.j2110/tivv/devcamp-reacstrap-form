import { Button, Col, Container, Input, Label, Row } from "reactstrap";
import reactImg from '../assets/images/reactstrap.jpg'

function ReactStrap() {
    return (
        <Container className="mt-3 border">
            <Row>
                <Col sm='12' className="text-center text-bolder">
                    <h4>HỒ SƠ NHÂN VIÊN</h4>
                </Col>
            </Row>
            <Row>
                <Col sm='8'>
                    <Row className="form-group py-3">
                        <Col sm='3'>
                            <Label>Họ và tên:</Label>
                        </Col>
                        <Col sm='9'>
                            <Input></Input>
                        </Col>
                    </Row>
                    <Row className="form-group py-3">
                        <Col sm='3'>
                            <Label>Ngày sinh:</Label>
                        </Col>
                        <Col sm='9'>
                            <Input></Input>
                        </Col>
                    </Row>
                    <Row className="form-group py-3">
                        <Col sm='3'>
                            <Label>Số điện thoại</Label>
                        </Col>
                        <Col sm='9'>
                            <Input></Input>
                        </Col>
                    </Row>
                    <Row className="form-group py-3">
                        <Col sm='3'>
                            <Label>Giới tính: </Label>
                        </Col>
                        <Col sm='9'>
                            <Input></Input>
                        </Col>
                    </Row>
                </Col>
                <Col sm='4' className="text-center">
                    <img src={reactImg} alt="reacimage.jpg" width='100%' className="img-thumbnail" />
                </Col>
            </Row>
            <Row className="py-2">
                <Col sm='2' className="form-group">
                    <Label>Công việc: </Label>
                </Col>
                <Col sm='10' className="form-group">
                    <textarea className="form-control"></textarea>
                </Col>
            </Row>
            <Row>
                <Col sm='12'  className="text-center mt-2">
                    <div>
                        <Button className="btn btn-success mx-2">Chi tiết</Button>
                        <Button className="btn btn-warning mx-2">Kiểm tra</Button>
                    </div>
                </Col>
            </Row>
        </Container>
    )
}
export default ReactStrap;